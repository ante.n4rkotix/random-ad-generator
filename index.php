<!DOCTYPE html>
<html lang="en">
  
<head>
    <title>Oglasi</title>
	
	<style>
		table, th, td {
		  border: 1px solid black;
		  border-collapse: collapse;
		}
	</style>
	
	<meta name="viewport" content="initial-scale=1.0, user-scalable=no"/> 
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
</head>
<script>

	$(document).ready(function(){
		var locations = ["Zagreb", "Split", "Zadar", "Osijek", "Rijeka"];
		var random = locations[Math.floor(Math.random() * locations.length)];
		
		$(".loadCity").val(random);
	
	})

</script>
<body>
    <center>
        <h1>Trenutno postojeći oglasi</h1>
			<div>
				<form action="loadads.php" method="post" enctype="multipart/form-data">
					<input type="button" value="Kreiraj oglas" onClick="document.location.href='adinput.php'" />
					<input type="submit" value="Simuliraj oglas" />
					<input type="hidden" class="loadCity" name="loadCity" value="">
				</form>
            </div>
				<?php
					$conn = mysqli_connect('localhost', 'root', '', 'stardigital');

					$query = "SELECT * FROM ads";
					$result = mysqli_query($conn, $query);

					echo "<table>";
						echo "<tr>
							<td>Naziv</td>
							<td>URL</td>
							<td>Budžet</td>
							<td>Akcija</td>
						</tr>";
					while($row = mysqli_fetch_array($result)){
						if($row['amount'] < 20){
							echo "<tr style='background-color:#ff8f8f'>";
						}else{
							echo "<tr>";
						}
					
					echo   "<td>" . $row['description'] . "</td>
							<td>" . $row['landing_url'] . "</td>
							<td>" . $row['amount'] . "</td>
							<td><a href=\"delete.php?id=".$row['id']."\">Delete</a></td>
						</tr>";
					}

					echo "</table>";

					mysqli_close($conn);
				?>        
        
    </center>
</body>
  
</html>