<!DOCTYPE html>
<html lang="en">
  
<head>
    <title>Unos oglasa</title>
</head>
  
<body>
    <center>
        <h1>Unesite podatke o oglasu</h1>
  
        <form action="insert.php" method="post" enctype="multipart/form-data">
              
            <p>
                <label for="name">Naslov:</label>
                <input type="text" name="name" id="name">
            </p>
  
            <p>
                <label for="ad_pictures">Slika:</label>
                <input type="file" name="ad_pictures" id="ad_pictures" accept="image/x-png,image/jpeg">
            </p>
  
            <p>
                <label for="landing_url">Landing URL:</label>
                <input type="text" name="landing_url" id="landing_url">
            </p>
  
            <p>
                <label for="starting_date">Početak oglašavanja:</label>
                <input type="date" name="starting_date" id="starting_date">
            </p>
  
			<p>
                <label for="ending_date">Završetak oglašavanja:</label>
                <input type="date" name="ending_date" id="ending_date">
            </p>
              
            <p>
                <label for="amount">Budžet:</label>
                <input type="number" name="amount" id="amount">
            </p>
			
			<p>
                <label for="city">Grad:</label>
                <?php
					$conn = mysqli_connect("localhost", "root", "", "stardigital") or die(mysqli_error());

					$query = "SELECT * from city";
					$result = mysqli_query($conn, $query) or die(mysqli_error()."[".$query."]");
					?>

					<select name="city[]" size="5" multiple>
					<option value="0">Odaberi gradove</option>
					<?php 
					while ($row = mysqli_fetch_array($result))
					{
						echo "<option value='".$row['id']."'>".$row['name']."</option>";
					}
				?>        
					</select>
            </p>
  
             <button type="submit" name="submit" class="btn btn-info">Upload</button>
            
        </form>
    </center>
</body>
  
</html>